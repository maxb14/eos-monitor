// IMPORTS
var express = require('express');
var router = express.Router();
var ctl = require('../controllers/conflictController');

// Get method : Call run controller with the following path : /conflict
router.get('/', ctl.confroot);

router.post('/', ctl.confstart);

module.exports = router;
