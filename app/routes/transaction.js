// IMPORTS
var express = require('express');
var router = express.Router();
var ctl = require('../controllers/transactionController');

// Get method : Call run controller with the following path : /transaction
router.get('/', ctl.transactionroot);

router.get('/:id', ctl.transactionshow);

module.exports = router;
