// IMPORTS
var express = require('express');
var router = express.Router();
var ctl = require('../controllers/rentController');

// Get method : Call run controller with the following path : /rent
router.get('/', ctl.rentroot);

router.post('/', ctl.rentrun);

module.exports = router;
