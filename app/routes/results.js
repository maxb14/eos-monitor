// IMPORTS
var express = require('express');
var router = express.Router();
var ctl = require('../controllers/resultsController');

// Get method : Call run controller with the following path : /results
router.get('/', ctl.resultsroot);

module.exports = router;
