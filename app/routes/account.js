// IMPORTS
var express = require('express');
var router = express.Router();
var ctl = require('../controllers/accountController');

// Get method : Call run controller with the following path : /account
router.get('/', ctl.accountroot);

router.post('/', ctl.accountshow);

router.get('/:name', ctl.accountshow);

module.exports = router;
