// IMPORTS
var express = require('express');
var router = express.Router();
var ctl = require('../controllers/runController');

// Get method : Call run controller with the following path : /run
router.get('/', ctl.runroot);

router.post('/', ctl.runstart);

module.exports = router;
