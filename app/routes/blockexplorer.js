// IMPORTS
var express = require('express');
var router = express.Router();
var ctl = require('../controllers/blockexplorerController');

router.get('/', ctl.blockexplorerroot);

router.post('/', ctl.blockexplorerresult);

router.get('/:num', ctl.blockexplorerresult);

module.exports = router;
