// IMPORTS
var express = require('express');
var router = express.Router();
var ctl = require('../controllers/registerController');

// Get method : Call run controller with the following path : /register
router.get('/', ctl.registerroot);

router.post('/', ctl.registerrun);

module.exports = router;
