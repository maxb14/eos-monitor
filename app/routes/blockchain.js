// IMPORTS
var express = require('express');
var router = express.Router();
var ctl = require('../controllers/blockchainController');

// Get method : Call run controller with the following path : /run
router.get('/', ctl.blockchainroot);

module.exports = router;
