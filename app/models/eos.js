// IMPORTS
Eos = require('eosjs')

const { Api, JsonRpc, RpcError } = require('eosjs');
const fetch = require('node-fetch');                            // node only; not needed in browsers
const { TextEncoder, TextDecoder } = require('util');           // node only; native TextEncoder/Decoder 
const host = "yourip";
const rpc = new JsonRpc(host, { fetch });
const JsSignatureProvider = require('eosjs/dist/eosjs-jssig'); 
const defaultPrivateKey = "5KQwrPbwdL6PhXujxW37FSSQZ1JiwsST4cqQzDeyXtP79zkvFD3";
const signatureProvider = new JsSignatureProvider.default([defaultPrivateKey]);
const api = new Api({ rpc, signatureProvider, textDecoder: new TextDecoder(), textEncoder: new TextEncoder() });

/**
 * model EOS
 */
module.exports = {
	getHost: function() {
		return host;
	},
	getInfo: function() {
		return rpc.get_info();
	},
	getBlock: function(num_block) {
		return rpc.get_block(num_block);
	},
	getBlockHeader: function(num_block) {
		return rpc.get_block_header_state(num_block);
	},
	getAccount: function(account_name) {
		return rpc.get_account(account_name);		
	},
	getAllBalance: function(code,acc_name) {
		return rpc.get_currency_balance(code,acc_name);
	},
	getBalance: function(code,acc_name,symbol) {
		return rpc.get_currency_balance(code,acc_name,symbol);
	},
	getCurrencyStats: function(code,symbol) {
		return rpc.get_currency_stats(code,symbol)
	},
	getEndPoint: function() {
		return rpc.endpoint.toString();
	},
	getActionHistory: function(acc_name) {
		return rpc.history_get_actions(acc_name)
	},
	getAbi: function(acc_name) {
		return rpc.get_abi(acc_name)
	},
	getTransaction(id) {
		return rpc.history_get_transaction(id)
	},
	getProducersSchedule: function() {
		return rpc.get_producer_schedule();
	},
	getControlledAccounts: function(acc_name) {
		return rpc.history_get_controlled_accounts(acc_name)
	},
	transfer: function(sender,receiver,quantity, symbol, memo) {
		return api.transact({
			  actions: [{
				account: 'eosio.token',
				name: 'transfer',
				authorization: [{
				  actor: sender,
				  permission: 'active',
				}],
				data: {
				  from: sender,
				  to: receiver,
				  quantity: quantity + '.0000 '+symbol,
				  memo: memo,
				},
			  }]
			}, {
			  blocksBehind: 3,
			  expireSeconds: 30,
		});
	},
	newaccount: function(acc_name) {
		return api.transact({
			actions: [{
				account: 'eosio',
				name: 'newaccount',
				authorization: [{
					actor: 'eosio',
					permission: 'active',
				}],
				data: {
					creator: 'eosio',
					name: acc_name,
					owner: {
						threshold: 1,
						keys: [{
							key: 'EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV',
							weight: 1
						}],
						accounts: [],
						waits: []
					},
					active: {
						threshold: 1,
						keys: [{
							key: 'EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV',
							weight: 1
						}],
						accounts: [],
						waits: []
					},
				},
			}]
		}, {
			blocksBehind: 3,
			expireSeconds: 30,
		});
	}
};