const ScatterJS = require('scatterjs-core');                
const ScatterEOS = require('scatterjs-plugin-eosjs2').default;    
ScatterJS.plugins( new ScatterEOS() );

ScatterJS.scatter.connect("poc").then(connected => {
    // User does not have Scatter Desktop, Mobile or Classic installed.
    if(!connected) return false;

    const scatter = ScatterJS.scatter;

    window.ScatterJS = null;
});