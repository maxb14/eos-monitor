// IMPORTS

var eosBlockchain = require('../models/eos');
 
/**
 * Display of the page run.twig
 */ 
exports.confroot = function(req, res) {
	res.render('conflict', { title: 'Declare conflict'});	
}


exports.confstart = function(req, res) {
	var data = req.body;
    var name = data.name;
    var conflict = data.conflict;
    var memo = data.memo;
    var vehicle = data.vehicle;

    var setTransfer = eosBlockchain.transfer(name, "eosio", 1, "BBD", name+" : "+conflict+" : "+vehicle+" : "+memo);

	setTransfer.then(result => {
		callback(result);
	}).catch(error => {
		callback(error);
	});	

	function callback(result) {
		console.log(result);
		res.render('conflict', { title: 'Declare conflict', result : result});
	}

}
