// IMPORTS

var eosBlockchain = require('../models/eos');
var beautify = require("json-beautify");

/**
 * Display of the page blockexplorer.twig
 */ 
exports.blockexplorerroot = function(req, res) {
	res.render('blockexplorer', { title: 'Block explorer', result: {}});	
}

exports.blockexplorerresult =  function(req, res) {
	var data = req.body;

	if (req.method == "POST") {
		var blocknum = data.blocknum;
	} else {
		var blocknum = req.params.num;
	}

	var getBlock = eosBlockchain.getBlock(blocknum);

	getBlock.then(result => {
		callback(result);
	}).catch(error => {
		callback(error);
	});	

	function callback(result) {
		res.render('blockexplorer', { title: 'Block explorer', result: result });	
	}
}

