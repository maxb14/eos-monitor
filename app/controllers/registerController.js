// IMPORTS

var eosBlockchain = require('../models/eos');
var beautify = require("json-beautify");

/**
 * Display of the page register.twig
 */ 
exports.registerroot = function(req, res) {
	res.render('register', { title: 'Register account'});	
}

exports.registerrun =  function(req, res) {
    var data = req.body;
	var name = data.name;
	
	var newAccount = eosBlockchain.newaccount(name);

	newAccount.then(result => {
		callback(result);
	}).catch(error => {
		callback(error);
	});	

	function callback(result) {
        console.log(result);
		res.render('register', { title: 'Register account', result: result});	
	}
}

