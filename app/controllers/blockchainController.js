// IMPORTS

var eosBlockchain = require('../models/eos');
var beautify = require("json-beautify");

/**
 * Display of the page blockchain.twig
 */ 
exports.blockchainroot = function(req, res) {
	var getConnect = eosBlockchain.getInfo();
	var host = eosBlockchain.getHost();

	getConnect.then(result => {
		callback(result);
	}).catch(error => {
		callback(error);
	});	

	function callback(result) {
		res.render('blockchain', { title: 'Blockchain info', result: result, host: host});	
	}
}

