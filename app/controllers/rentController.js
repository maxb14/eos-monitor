// IMPORTS

var eosBlockchain = require('../models/eos');
var beautify = require("json-beautify");

/**
 * Display of the page rent.twig
 */ 
exports.rentroot = function(req, res) {
	res.render('rent', { title: 'Rent'});	
}

exports.rentrun =  function(req, res) {
    var data = req.body;
	var customer = data.customer;
	var vehicle = data.vehicle;
	var date = data.date;
    var time = data.time;
	var memo = vehicle+" rented from "+customer+" at "+date+":"+time;

	var setTransfer = eosBlockchain.transfer(customer, "eosio", 1, "BBD", memo);

	setTransfer.then(result => {
		callback(result);
	}).catch(error => {
		callback(error);
	});	

	function callback(result) {
        console.log(result);
		res.render('rent', { title: 'Rent', result: result});	
	}
}

