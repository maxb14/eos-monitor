// IMPORTS

var eosBlockchain = require('../models/eos');
var beautify = require("json-beautify");

/**
 * Display of the page register.twig
 */ 
exports.transactionroot = function(req, res) {
	res.render('transaction', { title: 'Transaction explorer'});	
}

exports.transactionshow =  function(req, res) {
    var data = req.body;
	var txid = req.params.id;

	var getTransaction = eosBlockchain.getTransaction(txid);

	getTransaction.then(result => {
		callback(result);
	}).catch(error => {
		callback(error);
	});	

	function callback(result) {
        console.log(result);
		res.render('transaction', { title: 'Transaction explorer', result: result});	
	}
}