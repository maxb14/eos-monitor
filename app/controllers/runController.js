// IMPORTS

var eosBlockchain = require('../models/eos');
 
/**
 * Display of the page run.twig
 */ 
exports.runroot = function(req, res) {
	res.render('run', { title: 'Transfer token'});	
}


exports.runstart = function(req, res) {
	var data = req.body;
	var sender = data.sender;
	var receiver = data.receiver;
	var amount = data.amount;
	var symbol = data.symbol;
	var memo = amount+" "+symbol+" transfered from "+sender+" to "+receiver;

	var setTransfer = eosBlockchain.transfer(sender, receiver, amount, symbol, memo);

	setTransfer.then(result => {
		callback(result);
	}).catch(error => {
		callback(error);
	});	

	function callback(result) {
		console.log(result);
		res.render('run', { title: 'Transfer token', result : result});
	}

}
