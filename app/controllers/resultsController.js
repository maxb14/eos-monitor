// IMPORTS

var eosBlockchain = require('../models/eos');

 
/**
 * Display of the page results.twig
 */ 
exports.resultsroot = function(req, res) {
	res.render('results', { title: 'Results', result: {}});	
}