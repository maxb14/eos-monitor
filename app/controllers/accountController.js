// IMPORTS

var eosBlockchain = require('../models/eos');
 
/**
 * Display of the page account.twig
 */ 
exports.accountroot = function(req, res) {
	res.render('account', { title: 'Show account', result: {}});	
}

exports.accountshow = function(req, res) {
	var data = req.body;

	if (req.method == "POST") {
		var account = data.account;
	} else {
		var account = req.params.name;
	}

	var getAccount = eosBlockchain.getAccount(account);
	var getAllBalance = eosBlockchain.getAllBalance("eosio.token", account);
	var getActions = eosBlockchain.getActionHistory(account);

	getAccount.then(result => {
		getAllBalance.then(balances => {
			getActions.then(actions => {
				callback(result, balances, actions);
			}).catch(error => {
				callback(error);
			});
		}).catch(error => {
			callback(error);
		});	
	}).catch(error => {
		callback(error);
	});	

	function callback(result, balances, actions) {
		console.log(actions);
		res.render('account', { title: 'Show account', result: result, balances: balances, actions: actions});	
	}
}
