var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var Twig = require("twig");


var indexRouter = require('./app/routes/index');
var runRouter = require('./app/routes/run');
var blockchainRouter = require('./app/routes/blockchain');
var blockexplorerRouter = require('./app/routes/blockexplorer');
var accountRouter = require('./app/routes/account');
var resultsRouter = require('./app/routes/results');
var registerRouter = require('./app/routes/register');
var transactionRouter = require('./app/routes/transaction');
var rentRouter = require('./app/routes/rent');
var conflictRouter = require('./app/routes/conflict');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'app/views'));
app.set('view engine', 'twig');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/run', runRouter);
app.use('/blockchain', blockchainRouter);
app.use('/blockexplorer', blockexplorerRouter);
app.use('/account', accountRouter);
app.use('/results', resultsRouter);
app.use('/register', registerRouter);
app.use('/transaction', transactionRouter);
app.use('/rent', rentRouter);
app.use('/conflict', conflictRouter);

/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err,
            code: err.status
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
